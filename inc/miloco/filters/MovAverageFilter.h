/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <array>
#include <algorithm>

#include "miloco/miLoCo.h"


/**
 * Generic moving average filter
 */
template<
    typename data_t,
    std::size_t SIZE >
class Miloco::MovAverageFilter
{
    public:
        explicit MovAverageFilter() {
        }

        data_t
        init(data_t init_value) {
            std::fill(m_elements.begin(), m_elements.end(), init_value);

            m_accum = init_value * m_elements.size();

            return init_value;
        }

        data_t
        marchOneStep(data_t new_value) {
            m_accum = m_accum - m_elements[m_index] + new_value;
            m_elements[m_index] = new_value;

            m_index++;
            if (m_index == m_elements.size()) {
                m_index = 0;
            }

            return m_accum / static_cast<double>(m_elements.size());
        }

    private:
        std::array<data_t , SIZE> m_elements;
        data_t m_accum;
        std::size_t m_index = 0;
};
