/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <valarray>
#include <algorithm>

#include "miloco/miLoCo.h"


/**
 * Generic filter defined by a numerator and a denominator.
 *
 * The coefficients for both the numerator and denominator should be specified in
 * descending exponent order (z^2 + 2 * z + 1 -> {1, 2, 1}).
 * Please keep in mind that the grade of den. should be bigger than the grade of
 * num.
 */
template<typename data_t>
class Miloco::NumDen
{
    public:
        /**
         * Creates the filter based on numerator and denominator.
         * The output value at start is defined.
         *
         * \param num numerator in descending exponent order
         * \param den denominator in descending exponent order
         * \param initial_value the starting value of all elements in m_delayed_input
         */
        explicit NumDen(
            std::valarray<data_t> num,
            std::valarray<data_t> den,
            data_t initial_value)
            : m_num(den.size()),
              m_den(den / den[0]),
              m_delayed_input(initial_value, den.size()) {

            m_num = m_num + num / den[0];
            m_num = m_num.shift(num.size() - den.size());
        }

        /**
         * Creates the filter based on numerator and denominator
         *
         * \param num numerator in descending exponent order
         * \param den denominator in descending exponent order
         */
        explicit NumDen(
            std::valarray<data_t> num,
            std::valarray<data_t> den)
            : m_num(den.size()),
              m_den(den / den[0]),
              m_delayed_input(den.size()) {

            m_num = m_num + num / den[0];
            m_num = m_num.shift(num.size() - den.size());
        }

        /**
         * Insert a new value to the filter and return the output
         *
         * \param filter input
         * \return filter output
         */
        data_t marchOneStep(data_t new_value) {
            // Right shifting
            m_delayed_input = m_delayed_input.shift(-1);

            // Denominator calculation
            auto den_result = m_delayed_input * m_den;

            data_t* first_value = std::begin(m_delayed_input);
            *(first_value) = new_value - den_result.sum();

            // Numerator calculation
            auto num_result = m_delayed_input * m_num;

            return num_result.sum();
        }

    private:
        std::valarray<data_t> m_num;
        std::valarray<data_t> m_den;
        std::valarray<data_t> m_delayed_input;
};
