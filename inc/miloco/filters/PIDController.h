/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include "miloco/miLoCo.h"


/**
 * Generic PID controller.
 *
 * data_t should be float or double
 */
template<typename data_t>
class Miloco::PIDController
{
    public:
        explicit PIDController(data_t Kp, data_t Ki, data_t Kd)
            : a1(-0.99),
              b0(Kp + Ki + Kd),
              b1(-Kp - 2 * Kd),
              b2(Kd),
              z_1(0),
              z_2(0) {};

        data_t marchOneStep(data_t error) {
            data_t z_0 = error - a1 * z_1;
            data_t result = b2 * z_2 + b1 * z_1 + b0 * z_0;

            z_2 = z_1;
            z_1 = z_0;

            return result;
        }

    private:
        data_t a1, b0, b1, b2, z_1, z_2;
};

