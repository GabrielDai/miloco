/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <Eigen/Dense>

#include "miloco/miLoCo.h"


/**
 * State space representation.
 *
 * Please keep in mind the size of the matrices:
 * - state_matrix n x n
 * - input_matrix n x r
 * - output_matrix o x n
 * - direct_trans_matrix o x r
 *
 * Where:
 * - n -> amount of state variables
 * - r -> amount of inputs
 * - o -> amount of outputs
 */
template<
    std::size_t n,
    std::size_t r,
    std::size_t o,
    typename data_t>
class Miloco::StateSpaceSys
{
    // Selection of input and output types
    using input_t = Eigen::Matrix<data_t, r, 1>;
    using output_t = Eigen::Matrix<data_t, o, 1>;

    public:
        /**
         * It constructs a state space system with direct transmission matrix
         */
        explicit StateSpaceSys(
                Eigen::Matrix<data_t, n, n> state_matrix,
                Eigen::Matrix<data_t, n, r> input_matrix,
                Eigen::Matrix<data_t, o, n> output_matrix,
                Eigen::Matrix<data_t, o, r> direct_trans_matrix)
            : m_state_matrix(state_matrix),
              m_input_matrix(input_matrix),
              m_output_matrix(output_matrix),
              m_direct_trans_matrix(direct_trans_matrix),
              m_states(Eigen::Matrix<data_t, n, 1>::Zero()),
              m_last_output(Eigen::Matrix<data_t, o, 1>::Zero()) {
        }

        /**
         * It constructs a state space system without direct transmission matrix
         */
        explicit StateSpaceSys(
                Eigen::Matrix<data_t, n, n> state_matrix,
                Eigen::Matrix<data_t, n, r> input_matrix,
                Eigen::Matrix<data_t, o, n> output_matrix)
            : m_state_matrix(state_matrix),
              m_input_matrix(input_matrix),
              m_output_matrix(output_matrix),
              m_direct_trans_matrix(Eigen::Matrix<data_t, o, r>::Zero()),
              m_states(Eigen::Matrix<data_t, n, 1>::Zero()),
              m_last_output(Eigen::Matrix<data_t, o, 1>::Zero()) {
        }

        /**
         * It updates the output and the states based on a new input
         *
         * \param u_vector new input vector or scalar if there is one input
         */
        void setInput(input_t input) {
            // Input result: H*u[n]
            Eigen::Matrix<data_t, n, 1> input_res = m_input_matrix * input;

            // States result: G*x[n]
            Eigen::Matrix<data_t, n, 1> states_res = m_state_matrix * m_states;

            // New states: x[n+1] = G*x[n] + H*u[n]
            m_states = input_res + states_res;
        }

        /**
         * It returns the current states
         */
        Eigen::Matrix<data_t, n, 1> getStates() {
            return m_states;
        }

        /**
         * It returns the current output
         */
        output_t getOutput(input_t input) {
            // States output 1: C*x[n]
            output_t states_out = m_output_matrix * m_states;

            // Direct output 2: D*u[n]
            output_t direct_out = m_direct_trans_matrix * input;

            // New output: y[n] = C*x[n] + D*u[n]
            output_t output = states_out + direct_out;
            m_last_output = output;

            return output;
        }

        /**
         * It returns the last output
         */
        output_t getLastOutput() {
            return m_last_output;
        }

    private:
        Eigen::Matrix<data_t, n, n> m_state_matrix;
        Eigen::Matrix<data_t, n, r> m_input_matrix;
        Eigen::Matrix<data_t, o, n> m_output_matrix;
        Eigen::Matrix<data_t, o, r> m_direct_trans_matrix;

        Eigen::Matrix<data_t, n, 1> m_states;
        output_t m_last_output;
};
