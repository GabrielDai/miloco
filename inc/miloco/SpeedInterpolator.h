/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <array>

#include "miloco/miLoCo.h"


/**
 * SpeedInterpolator calculates the speed needed to go from the current point
 * to a new point
 */
template<
    typename data_points_updater_t,
    typename input_t,
    typename factor_t >
class Miloco::SpeedInterpolator
{
    public:
        using return_input_t = input_t;

        explicit SpeedInterpolator(data_points_updater_t updater)
            : m_updater(updater) {
            m_factor = m_updater.getFactor();
            m_index = m_factor;
        }


        /**
         * Calculate the speed between two points using m_factor
         *
         * \return result of the interpolation
         */
        input_t readSpeed() {
            if (m_index < m_factor - 1) {
                m_index++;
            } else {
                input_t current_value = m_updater.getCurrentPoint();
                input_t new_value = m_updater.getNewPoint();
                m_factor = m_updater.getFactor();

                m_speed = (new_value - current_value) / m_factor;

                m_index = 0;
            }

            return m_speed;
        }

    private:
        factor_t m_factor;
        factor_t m_index;
        input_t m_speed;
        data_points_updater_t m_updater;
};
