/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <atomic>
#include <chrono>
#include <time.h>
#include <pthread.h>

#include "miloco/miLoCo.h"
#include "miloco/tasks/Task.h"


/**
 * Implementation of a task executer for posix. One task requires one
 * TaskExecuter and this last one will create a thread to carry out the job
 */
class Miloco::Posix::TaskExecuter
{
    public:
        /**
         * The constructor receives the task to be executed
         *
         * \param task
         */
        explicit TaskExecuter(Miloco::Task& task);
        ~TaskExecuter();

        /**
         * This is the function to be called by the thread
         *
         * \param thread_obj_p represents the object instance
         */
        static void* run(void* thread_obj_p);

        /**
         * Create a thread with the desired attributes
         *
         * \param policy
         * \param priority
         * \return 0 in case no error has happend
         * \return -1 otherwise
         */
        int start(int policy, int priority);

        /**
         * Stop and join the thread with timeout
         *
         * \param timeout
         * \return 0 if the thread is ended within the timeout
         * \return -1 otherwise
         */
        int stop(std::chrono::nanoseconds timeout_ns);

    private:
        std::atomic<bool> m_running;
        Miloco::Task& m_task;
        timespec m_delay, m_rem_delay;

        pthread_t m_thread;
        pthread_attr_t m_thread_attr;
        sched_param m_thread_param;
};
