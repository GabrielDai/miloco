/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <array>

#include "miloco/miLoCo.h"


/**
 * Interpolator calculates the interpolation between consecutive samples
 */
template<
    typename data_points_updater_t,
    typename input_t,
    typename factor_t >
class Miloco::Interpolator
{
    public:
        using return_input_t = input_t;

        explicit Interpolator(data_points_updater_t updater)
            : m_updater(updater) {
            m_factor = m_updater.getFactor();
            m_index = m_factor;
        }


        /**
         * Calculate the interpolation between two elements
         *
         * \return result of the interpolation
         */
        input_t readPoint() {
            input_t result;

            if (m_index < m_factor - 1) {
                result = m_last_result + m_step;
                m_index++;
            } else {
                input_t current_value = m_updater.getCurrentPoint();
                input_t new_value = m_updater.getNewPoint();
                m_factor = m_updater.getFactor();

                m_step = (new_value - current_value) / m_factor;

                result = current_value + m_step;
                m_index = 0;
            }

            m_last_result = result;

            return result;
        }

    private:
        input_t m_last_result;
        factor_t m_factor;
        factor_t m_index;
        input_t m_step;
        data_points_updater_t m_updater;
};
