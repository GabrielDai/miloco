/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <chrono>
#include <Eigen/Dense>

#include "miloco/miLoCo.h"
#include "miloco/tasks/Task.h"


/**
 * Similar to PolePlacement, it implements the pole placement method but
 * additionally it makes use of an reducer-order observer.
 */
template<
    typename input_t,
    typename actuator_t,
    typename sensor_t,
    typename fdbk_matrix_t,
    typename observer_t>
class Miloco::ReducedObsPolePlacement: public Miloco::Task
{
    using data_t = typename input_t::data_t;

    public:
        explicit ReducedObsPolePlacement(
            std::chrono::microseconds delay,
            input_t setpoint,
            data_t pre_gain,
            actuator_t actuator,
            sensor_t sensors,
            observer_t observer,
            fdbk_matrix_t K)
            : Task(delay),
              m_setpoint(setpoint),
              m_pre_gain(pre_gain),
              m_actuator(actuator),
              m_sensors(sensors),
              m_observer(observer),
              m_K(K) {
        }

        /**
         * See Miloco::Task::setup()
         */
        void setup() override {
            m_actuator.setInitialValue();
            m_sensors.calibrate();
        }

        /**
         * See Miloco::Task::loop()
         */
        void loop() override {
            auto desired_input = m_setpoint.readValue();
            auto plant_output = m_sensors.readOutput();

            auto states = m_observer.getStates(plant_output);
            auto feedback = m_K * states;

            auto input = m_pre_gain * desired_input - feedback;
            m_observer.updateStates(input, plant_output);
            m_actuator.setPlantInput(input);
        }

    private:
        input_t m_setpoint;
        data_t m_pre_gain;
        actuator_t m_actuator;
        sensor_t m_sensors;
        observer_t m_observer;
        fdbk_matrix_t m_K;
};
