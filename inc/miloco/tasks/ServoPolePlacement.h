/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <chrono>
#include <Eigen/Dense>

#include "miloco/miLoCo.h"
#include "miloco/tasks/Task.h"


/**
 * Similar to PolePlacement but with a controller. If a integrator at the
 * origin is needed in order to reduce the steady-state error, this can
 * be added by the controller
 */
template<
    typename input_t,
    typename controller_t,
    typename actuator_t,
    typename sensor_t,
    typename fdbk_matrix_t>
class Miloco::ServoPolePlacement: public Miloco::Task
{
    using data_t = typename input_t::data_t;

    public:
        explicit ServoPolePlacement(
            std::chrono::microseconds delay,
            input_t setpoint,
            controller_t controller,
            actuator_t actuator,
            sensor_t sensors,
            fdbk_matrix_t K)
            : Task(delay),
              m_setpoint(setpoint),
              m_controller(controller),
              m_actuator(actuator),
              m_sensors(sensors),
              m_K(K) {
        }

        /**
         * See Miloco::Task::setup()
         */
        void setup() override {
            m_actuator.setInitialValue();
            m_sensors.calibrate();
        }

        /**
         * See Miloco::Task::loop()
         */
        void loop() override {
            auto desired_input = m_setpoint.readValue();
            auto plant_output = m_sensors.readOutput();

            auto error = desired_input - plant_output;
            auto controller_output = m_controller.marchOneStep(error);

            auto states = m_sensors.getStates();
            auto feedback = m_K * states;

            auto input = controller_output - feedback;

            m_actuator.setPlantInput(input);
        }

    private:
        input_t m_setpoint;
        controller_t m_controller;
        actuator_t m_actuator;
        sensor_t m_sensors;
        fdbk_matrix_t m_K;
};
