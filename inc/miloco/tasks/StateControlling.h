/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <chrono>

#include "miloco/miLoCo.h"
#include "miloco/tasks/Task.h"


/**
 * StateControlling is a task which implements a control loop for controlling
 * a variable
 */
template<
    typename input_t,
    typename controller_t,
    typename actuator_t,
    typename sensor_t >
class Miloco::StateControlling: public Miloco::Task
{
    public:
        explicit StateControlling(
            std::chrono::microseconds delay,
            input_t setpoint,
            controller_t controller,
            actuator_t actuator,
            sensor_t sensor)
            : Task(delay),
              m_setpoint(setpoint),
              m_controller(controller),
              m_actuator(actuator),
              m_sensor(sensor) {
        }

        /**
         * See Miloco::Task::setup()
         */
        void setup() override {
            m_actuator.setInitialValue();
            m_sensor.calibrate();
        }

        /**
         * See Miloco::Task::loop()
         */
        void loop() override {
            auto input = m_setpoint.readValue();
            auto output = m_sensor.readValue();

            auto error = input - output;
            auto plant_input = m_controller.marchOneStep(error);

            m_actuator.setPlantInput(plant_input);
        }

    private:
        input_t m_setpoint;
        controller_t m_controller;
        actuator_t m_actuator;
        sensor_t m_sensor;
};
