/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <chrono>

#include "miloco/miLoCo.h"
#include "miloco/tasks/Task.h"


/**
 * InputPreprocessing is a task used to filter the raw data of a input
 * in order to have smoothed values
 */
template<
    typename input_t,
    typename filter_t,
    typename filtered_input_t >
class Miloco::InputPreprocessing: public Miloco::Task
{
    public:
        explicit InputPreprocessing(
            std::chrono::microseconds delay,
            input_t input,
            filter_t filter,
            filtered_input_t preprocessed_input)
            : Task(delay),
              m_input(input),
              m_filter(filter),
              m_preprocessed_input(preprocessed_input) {
        }

        /**
         * See Miloco::Task::setup()
         */
        void setup() override {
            auto input = m_input.readValue();
            auto init_value = m_filter.init(input);
            m_preprocessed_input.updateValue(init_value);
        }

        /**
         * See Miloco::Task::loop()
         */
        void loop() override {
            auto input = m_input.readValue();
            auto filtered_input = m_filter.marchOneStep(input);

            m_preprocessed_input.updateValue(filtered_input);
        }

    private:
        input_t m_input;
        filter_t m_filter;
        filtered_input_t m_preprocessed_input;
};
