/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <chrono>

#include "miloco/miLoCo.h"


/**
 * Task groups a set of procedures, which are needed to be executed in a
 * period of time (delay + task execution time = period).
 */
class Miloco::Task
{
    public:
        explicit Task(std::chrono::microseconds delay)
            : m_delay(delay) {};

        virtual ~Task() {};

        /**
         * Procedures executed only once at the start of the task execution
         */
        virtual void setup() = 0;

        /**
         * Procedures executed periodically
         */
        virtual void loop() = 0;

        /**
         * Delay between the task execution
         */
        std::chrono::microseconds m_delay;
};
