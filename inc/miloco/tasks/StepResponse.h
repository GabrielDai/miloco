/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <chrono>

#include "miloco/miLoCo.h"
#include "miloco/tasks/Task.h"


/**
 * Task used for step response analysis of a plant.
 * The input is set when setup() is called and then the plant's output is logged
 */
template<
    typename input_t,
    typename actuator_t,
    typename sensor_t,
    typename logger_t>
class Miloco::StepResponse: public Miloco::Task
{
    public:
        explicit StepResponse(
            std::chrono::microseconds delay,
            input_t input,
            actuator_t actuator,
            sensor_t sensor,
            logger_t logger)
            : Task(delay),
              m_input(input),
              m_actuator(actuator),
              m_sensor(sensor),
              m_logger(logger) {
        }

        /**
         * See Miloco::Task::setup()
         */
        void setup() override {
            m_actuator.setInitialValue();
            m_sensor.calibrate();

            auto output = m_sensor.readValue();
            m_logger.save(output);

            auto plant_input = m_input.readValue();
            m_actuator.setPlantInput(plant_input);
        }

        /**
         * See Miloco::Task::loop()
         */
        void loop() override {
            auto output = m_sensor.readValue();
            m_logger.save(output);
        }

    private:
        input_t m_input;
        actuator_t m_actuator;
        sensor_t m_sensor;
        logger_t m_logger;
};
