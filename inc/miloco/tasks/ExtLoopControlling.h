/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <chrono>

#include "miloco/miLoCo.h"
#include "miloco/tasks/Task.h"


/**
 * ExtLoopControlling is a task which implements an external loop.
 * The result (m_inner_loop_crtl) could be used to control a
 * StateControlling task or another ExtLoopControlling
 */
template<
    typename input_t,
    typename controller_t,
    typename inner_loop_t,
    typename sensor_t >
class Miloco::ExtLoopControlling: public Miloco::Task
{
    public:
        explicit ExtLoopControlling(
            std::chrono::microseconds delay,
            input_t setpoint,
            controller_t controller,
            inner_loop_t preprocessed_input,
            sensor_t sensor)
            : Task(delay),
              m_setpoint(setpoint),
              m_controller(controller),
              m_inner_loop_crtl(preprocessed_input),
              m_sensor(sensor) {
        }

        /**
         * See Miloco::Task::setup()
         */
        void setup() override {
            m_inner_loop_crtl.setInitialValue();
            m_sensor.calibrate();
        }

        /**
         * See Miloco::Task::loop()
         */
        void loop() override {
            auto input = m_setpoint.readValue();
            auto output = m_sensor.readValue();

            auto error = input - output;
            auto inner_loop_input = m_controller.marchOneStep(error);

            m_inner_loop_crtl.updateValue(inner_loop_input);
        }

    private:
        input_t m_setpoint;
        controller_t m_controller;
        inner_loop_t m_inner_loop_crtl;
        sensor_t m_sensor;
};
