/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <Eigen/Dense>

#include "miloco/miLoCo.h"


/**
 * Reduced-order observer.
 *
 * Template data types:
 * - n -> amount of state variables
 * - m -> amount of state variables need not be estimated
 * - r -> amount of inputs
 * - o -> amount of outputs
 */
template<
    std::size_t n,
    std::size_t m,
    std::size_t r,
    std::size_t o,
    typename data_t>
class Miloco::ReducedObserver
{
    using input_t = Eigen::Matrix<data_t, r, 1>;
    using output_t = Eigen::Matrix<data_t, o, 1>;

    public:
        explicit ReducedObserver(
                Eigen::Matrix<data_t, m, m> Gaa,
                Eigen::Matrix<data_t, m, n-m> Gab,
                Eigen::Matrix<data_t, n-m, m> Gba,
                Eigen::Matrix<data_t, n-m, n-m> Gbb,
                Eigen::Matrix<data_t, m, r> Ha,
                Eigen::Matrix<data_t, n-m, r> Hb,
                Eigen::Matrix<data_t, n-m, o> Ke )
        : m_state_matrix(Gbb - Ke * Gab),
          m_input_matrix(Hb - Ke * Ha),
          m_output_meas_matrix(Gba - Ke * Gaa),
          m_Ke(Ke),
          m_obs_states(Eigen::Matrix<data_t, n-m, 1>::Zero()),
          m_estimated_states(Eigen::Matrix<data_t, n, 1>::Zero()) {
        }

        /**
         * It updates the estimated states.
         * The current output current output of the system is needed
         *
         * \param input is u_vector new input vector or scalar if there is one input
         * \param output is the current output of the system
         */
        void updateStates(input_t sys_input, output_t sys_output) {
            // Input result: (Hb-Ke*Ha)*u[n]
            Eigen::Matrix<data_t, n-m, 1> input_res = m_input_matrix * sys_input;

            // Output result 1: (Gba-Ke*Gaa)*y[n]
            Eigen::Matrix<data_t, n-m, 1> output_res = m_output_meas_matrix * sys_output;

            // States result: (Gbb-Ke*Gab)*͂(η[n]+Ke*y[n])
            Eigen::Matrix<data_t, n-m, 1> states_res = m_state_matrix * (m_obs_states + m_Ke * sys_output);

            // New states: η[n+1] = (Hb-Ke*Ha)*u[n] + (Gba-Ke*Gaa)*y[n] + (Gbb-Ke*Gab)*͂(η[n]+Ke*y[n])
            m_obs_states = input_res + output_res + states_res;
        }

        /**
         * It returns the current states
         */
        Eigen::Matrix<data_t, n, 1> getStates(output_t sys_output) {
            // Estimated states
            m_estimated_states << sys_output, m_obs_states + m_Ke * sys_output;

            return m_estimated_states;
        }

    private:
        Eigen::Matrix<data_t, n-m, n-m> m_state_matrix;
        Eigen::Matrix<data_t, n-m, r> m_input_matrix;
        Eigen::Matrix<data_t, n-m, m> m_output_meas_matrix;
        Eigen::Matrix<data_t, n-m, o> m_Ke;
        Eigen::Matrix<data_t, n-m, 1> m_obs_states;
        Eigen::Matrix<data_t, n, 1> m_estimated_states;
};
