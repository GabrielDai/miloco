/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <cstddef>


/**
 * Forward declaration of namespace Miloco
 */
namespace Miloco
{
    // Entities
    template<
        typename data_points_updater_t,
        typename input_t,
        typename factor_t >
    class Interpolator;

    template<
        typename data_points_updater_t,
        typename input_t,
        typename factor_t >
    class SpeedInterpolator;

    template<
        typename data_t,
        std::size_t SIZE >
    class MovAverageFilter;

    template<typename data_t>
    class PIDController;

    template<typename data_t>
    class NumDen;

    template<
        std::size_t n,
        std::size_t r,
        std::size_t o,
        typename data_t>
    class StateSpaceSys;

    template<
        std::size_t n,
        std::size_t m,
        std::size_t r,
        std::size_t o,
        typename data_t>
    class ReducedObserver;

    class Task;


    // Use cases
    template<
        typename input_t,
        typename actuator_t,
        typename sensor_t,
        typename logger_t>
    class StepResponse;

    template<
        typename input_t,
        typename filter_t,
        typename filtered_input_t >
    class InputPreprocessing;

    template<
        typename input_t,
        typename controller_t,
        typename actuator_t,
        typename sensor_t >
    class StateControlling;

    template<
        typename input_t,
        typename controller_t,
        typename inner_loop_t,
        typename sensor_t >
    class ExtLoopControlling;

    template<
        typename input_t,
        typename actuator_t,
        typename sensor_t,
        typename fdbk_matrix_t>
    class PolePlacement;

    template<
        typename input_t,
        typename controller_t,
        typename actuator_t,
        typename sensor_t,
        typename fdbk_matrix_t>
    class ServoPolePlacement;

    template<
        typename input_t,
        typename actuator_t,
        typename sensor_t,
        typename fdbk_matrix_t,
        typename observer_t>
    class ReducedObsPolePlacement;

    // Framework
    namespace Posix
    {
        class TaskExecuter;
    }
}
