# miLoCo - mini Loop Control library

It's a little library for the implementation of control loops licensed under LGPL v3.0

This library uses [eigen](http://eigen.tuxfamily.org) for some numerics computations.

## Features

miLoCo offers the following features and control algorithms:

* Filtering
* Step response
* Loop control (also nested loops)
* Pole placement method
* Reduced-order observer
* Plant simulation (by means of transfer function or state space representation)
* Interpolation

## Building

Most of the library are headers. But if you are using a POSIX compliance OS (there is some code ready to use) or if you want to run the
examples on your desktop computer just:

``` shell
mkdir build
cd build
cmake ..
```
and then `make pmiloco` for static library and `make miLoCo-examples` for examples.


**Note:** calling `cmake ..` will compile for the host architecture.
If you want to crosscompile, just give to cmake the path to toolchain file configuration
(e.g. `cmake -DCMAKE_TOOLCHAIN_FILE=/path/to/file ..`)

## Documentation

You can generate it using Doxygen. By default it will be created under doc/html.

## Concepts

### Tasks

These represent the operations needed for a desired control loop algorithm.
Tasks are run by a TaskExecuter. One implementation is given for POSIX.

Lets see an example base on **StateControlling** which is for a basic control loop:
* The called methods in setup() are executed at the beginning of the task execution only once.
In this example, they are:
```
m_actuator.setInitialValue();
m_sensor.calibrate();
```
* This methods are useful for example to set an absolute reference
* The called methods in loop() are executed repeatedly with a delay between executions.
They are:
```
auto input = m_setpoint.readValue();
auto output = m_sensor.readValue();

auto error = input - output;
auto plant_input = m_controller.marchOneStep(error);

m_actuator.setPlantInput(plant_input);
```
* What is happening here is easy: An error is calculated between the desired setpoint and
measurement of the sensor. This value is given to the controller and it returns another one
, which is used by the actuator to set the input to the plant

For a better understanding please run the examples.

### Entities

These are classes that need to be implemented by the one using the library. For example in case
of StateControlling, they are the following:
* input_t
* controller_t
* actuator_t
* sensor_t

Note that they are the typename declarations of the template.

The entities should implement some methods depending on the task. Please see the examples.


## Use of the library

As you may notice, most of the library consist of header files. For this reason it is highly
probably you won't need to compile into a static library. In this case, as is described in
section 3 of LGPL, you should mention "prominently" that your application uses miLoCo. For me
it will be enough to add a comment with a link to this project on the files in where headers
of miLoCo are included.



**Thanks for reading this file and long live programming!**
