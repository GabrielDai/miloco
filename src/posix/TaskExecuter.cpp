/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#include <chrono>
#include <iostream>

#include "miloco/posix/TaskExecuter.h"
#include "miloco/posix/Utils.h"


Miloco::Posix::TaskExecuter::TaskExecuter(Miloco::Task& task)
    : m_running(true),
      m_task(task)
{
    pthread_attr_init(&m_thread_attr);
}

Miloco::Posix::TaskExecuter::~TaskExecuter()
{
}

void* Miloco::Posix::TaskExecuter::run(void* thread_obj_p)
{
    auto thread_obj = static_cast<Miloco::Posix::TaskExecuter*>(thread_obj_p);
    thread_obj->m_task.setup();

    while(thread_obj->m_running) {
        auto t1 = std::chrono::steady_clock::now();
        thread_obj->m_task.loop();
        auto t2 = std::chrono::steady_clock::now();

        thread_obj->m_delay = Utils::Time::chronoDurationToTimespec(
                thread_obj->m_task.m_delay - (t2 - t1));

        nanosleep(
                &(thread_obj->m_delay),
                &(thread_obj->m_rem_delay));
    }

    return NULL;
}

int Miloco::Posix::TaskExecuter::start(int policy, int priority)
{
    int result;

    const int max_prio = sched_get_priority_max(policy);
    const int min_prio = sched_get_priority_min(policy);

    if((priority < min_prio) ||
       (priority > max_prio)) {
        std::cerr << "[Error] wrong priority" << std::endl;
        return -1;
    }
    m_thread_param.sched_priority = priority;

    result = pthread_attr_setschedpolicy(&m_thread_attr, policy);
    if (result) {
        std::cerr << "[Error] pthread_attr_setschedpolicy: " << result << std::endl;
        return result;
    }

    result = pthread_attr_setschedparam(&m_thread_attr, &m_thread_param);
    if (result) {
        std::cerr << "[Error] pthread_attr_setschedparam: " << result << std::endl;
        return result;
    }

    result = pthread_create(
             &m_thread,                         // thread's id
             &m_thread_attr,                    // thread's attributes
             Miloco::Posix::TaskExecuter::run,  // thread's function
             this);                             // arguments

    pthread_attr_destroy(&m_thread_attr);

    return result;
}

int Miloco::Posix::TaskExecuter::stop(std::chrono::nanoseconds timeout_ns)
{
    auto timeout = Utils::Time::chronoDurationToTimespec(timeout_ns);

    m_running = false;

    void* retval;
    return pthread_timedjoin_np(m_thread, &retval, &timeout);
}
