/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#include "miloco/posix/Utils.h"


timespec Utils::Time::chronoDurationToTimespec(std::chrono::nanoseconds duration_ns)
{
    using namespace std::chrono_literals;

    std::chrono::seconds duration_s = 0s;
    if (duration_ns >= 1s) {
        duration_s = std::chrono::duration_cast<std::chrono::seconds>(duration_ns);
        duration_ns -= duration_s;
    }
    timespec duration_spec = {
        static_cast<__time_t>(duration_s.count()),
        static_cast<__syscall_slong_t>(duration_ns.count())};

    return duration_spec;
}
