/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#include "EntitiesExamples.h"
#include "miloco/tasks/InputPreprocessing.h"
#include "miloco/posix/TaskExecuter.h"


using namespace std::chrono_literals;


/**
 * Example that shows how to use miLoCo for filtering an input and share the
 * filtered value
 */
int main (int argc, char* argv[])
{
    // Objects needed for the task execution
    MyInput input(7);
    MyPreprocessedInput pre_input;
    MyFilter filter;

    // Tasks creation
    using InputPreprocessing_t = Miloco::InputPreprocessing<
        MyInput,
        MyFilter,
        MyPreprocessedInput>;

    InputPreprocessing_t input_preprocessing(
        500ms,
        input,
        filter,
        pre_input);

    // Task executer
    Miloco::Posix::TaskExecuter task_executer(
        input_preprocessing);

    // Starting task executer
    task_executer.start(SCHED_FIFO, 20);

    std::cout << "Type something and then press enter to exit" << std::endl;
    std::string wait;
    std::cin >> wait;

    // Stopping task executer
    task_executer.stop(1s);

    return 0;
}
