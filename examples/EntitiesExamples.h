/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include <atomic>
#include <cstdint>
#include <array>
#include <iostream>
#include <Eigen/Dense>

#include "miloco/Interpolator.h"


/**
 * Example entity used as input
 */
class MyInput
{
    public:
        explicit MyInput(float value) : m_first_time(true), m_value(value) {}

        float readValue() const{
            if(m_first_time) {
                m_first_time = false;
                return -m_value;
            }

            return m_value;
        }

    private:
        mutable bool m_first_time;
        float m_value;
};


/**
 * Another input example (This is used in PolePlacementExample)
 */
class MyInput2
{
    public:
        using data_t = double;
        static constexpr std::size_t inputs = 1;

        explicit MyInput2(double value) {
            m_value << value;
        }

        Eigen::Matrix<double, inputs, 1> readValue() const {
            return m_value;
        }

    private:
        Eigen::Matrix<double, inputs, 1> m_value;
};


/**
 * Example entity used in the filter example. It implements a moving average
 * filter of 2 elements
 */
class MyFilter
{
    public:
        explicit MyFilter() {}

        float init(float init_value) {
            marchOneStep(init_value);
            return marchOneStep(init_value);
        }

        float marchOneStep(float new_value) {
            float result = (new_value + m_last_value) / 2;
            m_last_value = new_value;

            return result;
        }

    private:
        float m_last_value;
};


/**
 * Example entity used in the filter example. It represents the filtered input
 */
class MyPreprocessedInput
{
    public:
        explicit MyPreprocessedInput() {}

        void updateValue(float new_value) {
            std::cout << "Filter output: " << new_value << std::endl;
        }
};


/**
 * Example entity used in examples: one loop example, double loop example and
 * interpolation example. It implements a On/Off Histeresis controller
 */
class MyController
{
    public:
        explicit MyController(
            float h_threshold,
            float l_threshold)
        : m_h_threshold(h_threshold),
          m_l_threshold(l_threshold) {}

        bool marchOneStep(float error) {
            bool out = last_out;
            if(error >= m_h_threshold) {
                out = true;
            }

            if(error <= m_l_threshold) {
                out = false;
            }

            last_out = out;
            return out;
        }

    private:
        float m_h_threshold;
        float m_l_threshold;
        bool last_out = 0;
};


/**
 * Example entity used in ServoPolePlacementExample. It implements a integral controller
 */
class MyController2
{
    public:
        explicit MyController2(double gain)
            : m_gain(gain),
              m_last_out(0) {}

        Eigen::Matrix<double, 1, 1> marchOneStep(Eigen::Matrix<double, 1, 1> error) {
            Eigen::Matrix<double, 1, 1> out;
            out << error + m_last_out;
            m_last_out << out;

            return m_gain * out;
        }

    private:
        double m_gain;
        Eigen::Matrix<double, 1, 1> m_last_out;
};


/**
 * Example entity used in examples: one loop example, double loop example and
 * interpolation example. It implements a On/Off actuator
 */
class MyActuator
{
    public:
        explicit MyActuator() {}

        void setInitialValue() {
            std::cout << "Plant input: ON" << std::endl;
        }

        void setPlantInput(bool input) {
            if (input) {
                std::cout << "Plant input: ON" << std::endl;
            } else {
                std::cout << "Plant input: OFF" << std::endl;
            }
        }
};


/**
 * Example entity used in PolePlacementExample and ServoPolePlacementExample.
 * It represents the actuator of a simulated plant using Miloco::StateSpaceSys
 */
template<typename sys_t>
class MySysActuator
{
    public:
        explicit MySysActuator(sys_t& sys)
        : m_sys(sys) {}

        void setInitialValue() {
        }

        void setPlantInput(Eigen::Matrix<double, 1, 1> input) {
            m_sys.setInput(input);

            std::cout << "Plant output: " << m_sys.getOutput(input) << "\n";
        }

    private:
        sys_t& m_sys;
};


/**
 * Example entity used in examples: one loop example, double loop example and
 * interpolation example. It implements a sensor
 */
class MySensor
{
    public:
        explicit MySensor() : m_index(0), m_offset(0) {}

        void calibrate() {
            // Example calibration
            m_offset = readValue();
        }

        float readValue() const{
            float value;

            switch(m_index) {
                case 0:
                    value = 2.0;
                    break;

                case 1:
                    value = 11.0;
                    break;
            }

            m_index++;
            if(m_index == 2) {
                m_index = 0;
            }

            return (value - m_offset);
        }

    private:
        mutable uint8_t m_index;
        float m_offset;
};


/**
 * Example entity used in PolePlacementExample and ServoPolePlacementExample.
 * It represents the sensors of a simulated plant using Miloco::StateSpaceSys
 */
template<
    std::size_t n,
    typename sys_t>
class MySysSensors
{
    public:
        explicit MySysSensors(sys_t& sys)
        : m_sys(sys) {}

        void calibrate() {
        }

        Eigen::Matrix<double, n, 1> getStates() {
            return m_sys.getStates();
        }

        Eigen::Matrix<double, 1, 1> readOutput() {
            return m_sys.getLastOutput();
        }

    static constexpr std::size_t state_variables = n;

    private:
        sys_t& m_sys;
};


/**
 * Example entity used in the interpolation example. It implements the class,
 * which updates the data point needed for interpolation
 */
class DataPointsUpdater
{
    static constexpr uint8_t amountElements = 50;

    public:
        using samples_t = std::array<float, amountElements>;

        explicit DataPointsUpdater(float factor)
            : m_factor(factor),
              m_index(0) {
            float step = 100;

            for (uint8_t i = 0; i < m_samples.size()/2; i++) {
                m_samples[i] = step + static_cast<float>(i) * step;
            }
            for (uint8_t i = m_samples.size()/2; i < m_samples.size(); i++) {
                m_samples[i] = m_samples[i - 1] - step;
            }
        }

        float getFactor() const {
            return m_factor;
        }

        float getCurrentPoint() {
            float point = m_samples[m_index];

            m_index++;
            if (m_index == m_samples.size()) {
                m_index = 0;
            }

            return point;
        }

        float getNewPoint() {
            // In order to get the new point this function should be called
            // after getCurrentPoint() always
            return m_samples[m_index];
        }

    private:
        float m_factor;
        std::size_t m_index;
        samples_t m_samples;
};

/**
 * This is only a wrapper of Miloco::Interpolator that prints the
 * interpolated value
 */
class MyInterpolator
{
    public:
        using return_input_t = float;

        explicit MyInterpolator()
            : m_updater(5), // try changing this value (1 = fastest)
              m_interpolator(m_updater) {};

        float readValue() {
            float result = m_interpolator.readPoint();
            std::cout << "Interpolation result: " << result << std::endl;

            return result;
        }

    private:
        DataPointsUpdater m_updater;
        Miloco::Interpolator<
            DataPointsUpdater,
            return_input_t,
            float> m_interpolator;
};


/**
 * Example entity used in the double loop example. It implements a PID controller
 */
class MyPIDController
{
    public:
        explicit MyPIDController(float Kp, float Ki, float Kd)
            : a1(-0.99),
              b0(Kp + Ki + Kd),
              b1(-Kp - 2 * Kd),
              b2(Kd),
              z_1(0),
              z_2(0) {};

        float marchOneStep(float error) {
            float z_0 = error - a1 * z_1;
            float result = b2 * z_2 + b1 * z_1 + b0 * z_0;

            z_2 = z_1;
            z_1 = z_0;

            return result;
        }

    private:
        float a1, b0, b1, b2, z_1, z_2;
};


/**
 * Example entity used in the double loop example. It implements the a shared
 * signal, which is used for the external loop to control the internal loop
 */
class MySharedVariable
{
    public:
        explicit MySharedVariable() {}

        void setInitialValue() {
            std::cout << "Inner loop ctrl signal initialized" << std::endl;
        }

        float readValue() const{
            return m_value;
        }

        void updateValue(float new_value) {
            m_value = new_value;
            std::cout << "Inner loop ctrl signal: " << new_value << std::endl;
        }

    private:
        std::atomic<float> m_value;
};
