/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#include <iostream>
#include <Eigen/Dense>

#include "miloco/ReducedObserver.h"
#include "miloco/filters/StateSpaceSys.h"


using one_IO_sys_t = Miloco::StateSpaceSys<3, 1, 1, double>;


/**
 * This example compares the states of a simulated plant with the estimated
 * states of a reduced-order observer
 */
int main (int argc, char* argv[])
{
    // Plant
    Eigen::Matrix<double, 3, 3> G;
    G << 1, 0.009484, 0.00003012,
         0, 0.8638, 0.0046,
         0, -20.7793, 0.1179;

    Eigen::Matrix<double, 3, 1> H;
    H << 0.00631, 1.668, 254.5473;

    Eigen::Matrix<double, 1, 3> C;
    C << 1, 0, 0;

    one_IO_sys_t plantSS(G, H, C);


    // Observer
    Eigen::Matrix<double, 1, 1> Gaa;
    Gaa << 1;
    Eigen::Matrix<double, 1, 2> Gab;
    Gab << 0.009485, 0.000030121;
    Eigen::Matrix<double, 2, 1> Gba;
    Gba << 0, 0;
    Eigen::Matrix<double, 2, 2> Gbb;
    Gbb << 0.86383, 0.004596, -20.7794, 0.1179;

    Eigen::Matrix<double, 1, 1> Ha;
    Ha << 0.00630979;
    Eigen::Matrix<double, 2, 1> Hb;
    Hb << 1.66808774, 254.54730135;

    Eigen::Matrix<double, 2, 1> Ke;
    Ke << 105.812974098386, -728.643381325179;

    Miloco::ReducedObserver<
        3, 1, 1, 1,
        double> obs(Gaa, Gab, Gba, Gbb, Ha, Hb, Ke);

    // Comparison
    Eigen::Matrix<double, 1, 1> input_vector;
    input_vector << 1;
    Eigen::Matrix<double, 1, 1> output_vector;
    output_vector << plantSS.getOutput(input_vector);

    auto states = plantSS.getStates();
    auto estimated_states = obs.getStates(output_vector);

    for(int i = 0; i < 10; i++) {
        states = plantSS.getStates();
        std::cout << "Plant states: " << states[0] << ", " << states[1] << ", " << states[2] << "\n";

        output_vector << plantSS.getOutput(input_vector);
        estimated_states = obs.getStates(output_vector);
        std::cout << "Observer states: " << estimated_states[0] << ", " << estimated_states[1] << ", " << estimated_states[2] << "\n";
        std::cout << "\n";

        obs.updateStates(input_vector, output_vector);
        plantSS.setInput(input_vector); // states[n+1]
    }

    return 0;
}
