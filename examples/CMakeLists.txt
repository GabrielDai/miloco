 ##############################################################################
 #
 # miLoCo
 # Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 #
 # This file is part of miLoCo.
 #
 # miLoCo is free software: you can redistribute it and/or modify
 # it under the terms of the GNU Lesser General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # miLoCo is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU Lesser General Public License for more details.
 #
 # You should have received a copy of the GNU Lesser General Public License
 # along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 #
 ##############################################################################

add_custom_target( miLoCo-examples
    DEPENDS FilterExample
            OneLoopExample
            InterpolationExample
            DoubleLoopExample
            PlantExample
            PolePlacementExample
            ServoPolePlacementExample
            ReducedObserverExample
            ReducedObsPolePlacementExample
    )

set( EXAMPLES_HEADER_FILES
     ../inc
     ../contrib/eigen
    )

## Filter example --------------------------------------------------------------
set( EXAMPLES_SRC_FILES
    ./FilterExample.cpp
    )

add_executable( FilterExample ${EXAMPLES_SRC_FILES})

target_include_directories( FilterExample PUBLIC ${EXAMPLES_HEADER_FILES})

target_link_libraries( FilterExample PUBLIC pmiloco)

## One loop example ------------------------------------------------------------
set( EXAMPLES_SRC_FILES
    ./OneLoopExample.cpp
    )

add_executable( OneLoopExample ${EXAMPLES_SRC_FILES})

target_include_directories( OneLoopExample PUBLIC ${EXAMPLES_HEADER_FILES})

target_link_libraries( OneLoopExample PUBLIC pmiloco)

## Interpolation example ------------------------------------------------------
set( EXAMPLES_SRC_FILES
    ./InterpolationExample.cpp
    )

add_executable( InterpolationExample ${EXAMPLES_SRC_FILES})

target_include_directories( InterpolationExample PUBLIC ${EXAMPLES_HEADER_FILES})

target_link_libraries( InterpolationExample PUBLIC pmiloco)

## Double loop example ---------------------------------------------------------
set( EXAMPLES_SRC_FILES
    ./DoubleLoopExample.cpp
    )

add_executable( DoubleLoopExample ${EXAMPLES_SRC_FILES})

target_include_directories( DoubleLoopExample PUBLIC ${EXAMPLES_HEADER_FILES})

target_link_libraries( DoubleLoopExample PUBLIC pmiloco)

## Plant example ---------------------------------------------------------------
set( EXAMPLES_SRC_FILES
    ./PlantExample.cpp
    )

add_executable( PlantExample ${EXAMPLES_SRC_FILES})

target_include_directories( PlantExample PUBLIC ${EXAMPLES_HEADER_FILES})

target_link_libraries( PlantExample PUBLIC pmiloco)

## Pole placement example ------------------------------------------------------
set( EXAMPLES_SRC_FILES
    ./PolePlacementExample.cpp
    )

add_executable( PolePlacementExample ${EXAMPLES_SRC_FILES})

target_include_directories( PolePlacementExample PUBLIC ${EXAMPLES_HEADER_FILES})

target_link_libraries( PolePlacementExample PUBLIC pmiloco)

## Pole placement with servo example -------------------------------------------
set( EXAMPLES_SRC_FILES
    ./ServoPolePlacementExample.cpp
    )

add_executable( ServoPolePlacementExample ${EXAMPLES_SRC_FILES})

target_include_directories( ServoPolePlacementExample PUBLIC ${EXAMPLES_HEADER_FILES})

target_link_libraries( ServoPolePlacementExample PUBLIC pmiloco)

## Reduced-order observer example ----------------------------------------------
set( EXAMPLES_SRC_FILES
    ./ReducedObserverExample.cpp
    )

add_executable( ReducedObserverExample ${EXAMPLES_SRC_FILES})

target_include_directories( ReducedObserverExample PUBLIC ${EXAMPLES_HEADER_FILES})

target_link_libraries( ReducedObserverExample PUBLIC pmiloco)

## Pole placement with a reduced-order observer example ------------------------
set( EXAMPLES_SRC_FILES
    ./ReducedObsPolePlacementExample.cpp
    )

add_executable( ReducedObsPolePlacementExample ${EXAMPLES_SRC_FILES})

target_include_directories( ReducedObsPolePlacementExample PUBLIC ${EXAMPLES_HEADER_FILES})

target_link_libraries( ReducedObsPolePlacementExample PUBLIC pmiloco)
