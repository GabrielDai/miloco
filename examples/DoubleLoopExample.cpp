/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#include "EntitiesExamples.h"
#include "miloco/tasks/StateControlling.h"
#include "miloco/tasks/ExtLoopControlling.h"
#include "miloco/posix/TaskExecuter.h"


using namespace std::chrono_literals;


/**
 * Example that shows how to use miLoCo for two control loops
 */
int main (int argc, char* argv[])
{
    // Objects needed for the tasks execution
    MyController controller(5.5, 1.5);
    MyActuator actuator;
    MySensor sensor1, sensor2;
    MyInput input(7);
    MyPIDController PIDcontroller(1, 0.5, 0.5);
    MySharedVariable inner_loop_ctrl;

    // Tasks creation
    using StateControlling_t = Miloco::StateControlling<
        MySharedVariable&,
        MyController,
        MyActuator,
        MySensor>;

    StateControlling_t state_controlling(
        500ms,
        inner_loop_ctrl,
        controller,
        actuator,
        sensor1);

    using ExtLoopControlling_t = Miloco::ExtLoopControlling<
        MyInput,
        MyPIDController,
        MySharedVariable&,
        MySensor>;

    ExtLoopControlling_t ext_loop_controlling(
        2500ms,
        input,
        PIDcontroller,
        inner_loop_ctrl,
        sensor2);

    // Task executers
    Miloco::Posix::TaskExecuter task_executer_1(
        state_controlling);
    Miloco::Posix::TaskExecuter task_executer_2(
        ext_loop_controlling);

    // Starting task executers
    task_executer_1.start(SCHED_FIFO, 20);
    task_executer_2.start(SCHED_FIFO, 30);

    std::cout << "Type something and then press enter to exit" << std::endl;
    std::string wait;
    std::cin >> wait;

    // Stopping task executers
    task_executer_1.stop(1s);
    task_executer_2.stop(1s);

    return 0;
}
