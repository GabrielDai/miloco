/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#include "EntitiesExamples.h"
#include "miloco/tasks/StateControlling.h"
#include "miloco/posix/TaskExecuter.h"


using namespace std::chrono_literals;


/**
 * Example that shows how to use miLoCo for a control loop
 */
int main (int argc, char* argv[])
{
    // Objects needed for the task execution
    MyInput input(7);
    MyController controller(5.5, 1.5);
    MyActuator actuator;
    MySensor sensor;

    // Tasks creation
    using StateControlling_t = Miloco::StateControlling<
        MyInput,
        MyController,
        MyActuator,
        MySensor>;

    StateControlling_t state_controlling(
        500ms,
        input,
        controller,
        actuator,
        sensor);

    // Task executer
    Miloco::Posix::TaskExecuter task_executer(
        state_controlling);

    // Starting task executer
    task_executer.start(SCHED_FIFO, 20);

    std::cout << "Type something and then press enter to exit" << std::endl;
    std::string wait;
    std::cin >> wait;

    // Stopping task executer
    task_executer.stop(1s);

    return 0;
}
