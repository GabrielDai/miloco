/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#include <array>
#include <iostream>

#include "EntitiesExamples.h"
#include "miloco/ReducedObserver.h"
#include "miloco/filters/StateSpaceSys.h"
#include "miloco/tasks/ReducedObsPolePlacement.h"
#include "miloco/posix/TaskExecuter.h"


using namespace std::chrono_literals;
using one_IO_sys_t = Miloco::StateSpaceSys<3, 1, 1, double>;


/**
 * Example that shows how to use miLoCo for controlling a plant with
 * pole placement method with an reduced-order observer
 */
int main (int argc, char* argv[])
{
    // Simulated plant
    constexpr int state_variables = 3;

    // State matrix
    Eigen::Matrix<double, state_variables, state_variables> G;
    G << 0, 1, 0,
         0, 0, 1,
         -0.5, -0.2, 1.1;

    // Input matrix
    Eigen::Matrix<double, state_variables, 1> H;
    H << 0, 0, 1;

    // Output matrix
    Eigen::Matrix<double, 1, state_variables> C;
    C << 0, 1, 0;

    one_IO_sys_t plantSS(G, H, C);

    // Objects needed for the task execution
    MyInput2 input(1);
    MySysActuator<one_IO_sys_t> actuator(plantSS);
    MySysSensors<state_variables, one_IO_sys_t> sensors(plantSS);

    Eigen::Matrix<double, 1, state_variables> K;
    K << -0.2, -0.5, 1.1;

    // Observer
    Eigen::Matrix<double, 1, 1> Gaa;
    Gaa << 0;
    Eigen::Matrix<double, 1, 2> Gab;
    Gab << 0, 1;
    Eigen::Matrix<double, 2, 1> Gba;
    Gba << 1, -0.2;
    Eigen::Matrix<double, 2, 2> Gbb;
    Gbb << 0, 0, -0.5, 1.1;

    Eigen::Matrix<double, 1, 1> Ha;
    Ha << 0;
    Eigen::Matrix<double, 2, 1> Hb;
    Hb << 0, 1;

    Eigen::Matrix<double, 2, 1> Ke;
    Ke << 0, 1.1;

    using observer_t = Miloco::ReducedObserver<state_variables, 1, 1, 1, double>;

    observer_t obs(Gaa, Gab, Gba, Gbb, Ha, Hb, Ke);

    // Tasks creation
    using pole_placement_t = Miloco::ReducedObsPolePlacement<
        MyInput2,
        MySysActuator<one_IO_sys_t>,
        MySysSensors<state_variables, one_IO_sys_t>,
        Eigen::Matrix<double, 1, state_variables>,
        observer_t>;

    pole_placement_t obs_pole_placement(
        500ms,
        input,
        1,
        actuator,
        sensors,
        obs,
        K);

    // Task executer
    Miloco::Posix::TaskExecuter task_executer(
        obs_pole_placement);

    // Starting task executer
    task_executer.start(SCHED_FIFO, 20);

    std::cout << "Type something and then press enter to exit" << std::endl;
    std::string wait;
    std::cin >> wait;

    // Stopping task executer
    task_executer.stop(1s);

    return 0;
}
