/*
 * miLoCo
 * Copyright (C) 2020  Gabriel Moyano <vgmoyano@gmail.com>
 *
 * This file is part of miLoCo.
 *
 * miLoCo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * miLoCo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with miLoCo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#include <cstdint>
#include <iostream>
#include <Eigen/Dense>

#include "miloco/filters/NumDen.h"
#include "miloco/filters/StateSpaceSys.h"

/**
 * Example that shows how to simulate an output plant using NumDen
 * and StateSpaceSys
 */
int main (int argc, char* argv[])
{
    // Plant simulation using NumDen
    std::cout << "Plant output using NumDen:\n";

    Miloco::NumDen<double> plant(
        {1.66808774, 0.97341609},
        {1.        , -0.98167971,  0.19731267});

    for(uint16_t i = 0; i < 100; i++) {
        std::cout << plant.marchOneStep(1) << ", ";
    }
    std::cout << "\n";

    // Plant simulation using StateSpaceSys
    std::cout << "\nPlant output using StateSpaceSys:\n";

    // State matrix
    Eigen::Matrix<double, 2, 2> G;
    G << 0.8638, 0.0046, -20.7793, 0.1179;

    // Input matrix
    Eigen::Matrix<double, 2, 1> H;
    H << 1.668, 254.5473;

    // Output matrix
    Eigen::Matrix<double, 1, 2> C;
    C << 1, 0;

    Miloco::StateSpaceSys<2, 1, 1, double> plantSS(G, H, C);

    // Input
    Eigen::Matrix<double, 1, 1> u(1);

    for(uint16_t i = 0; i < 100; i++) {
        std::cout << plantSS.getOutput(u) << ", ";
        plantSS.setInput(u);
    }
    std::cout << "\n";

    return 0;
}
